import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Medium from './icons/medium.svg'
import Telegram from './icons/telegram.svg'
import Twitter from './icons/twitter.svg'
import './AppFooter.scss'

function AppFooter ({ className }) {
  return (
    <div className={classnames('app-footer', className)}>
      <Container className="app-footer__container">
        <Row className="align-items-center">
          <Col className="text-left">
            2020 All rights reserved
            &nbsp;
            <a href="https://finance.vote" target="_blank" rel="noreferrer">finance.vote</a>
          </Col>
          <Col className="app-footer__links text-right">
            <a href="https://t.me/financedotvote" target="_blank" rel="noreferrer">
              <img src={Telegram} alt="Telegram" />
            </a>
            &nbsp;
            <a href="https://twitter.com/financedotvote" target="_blank" rel="noreferrer">
              <img src={Twitter} alt="Twitter" />
            </a>
            &nbsp;
            <a href="https://medium.com/@financedotvote" target="_blank" rel="noreferrer">
              <img src={Medium} alt="Medium" />
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

AppFooter.propTypes = {
  className: PropTypes.string
}

export default memo(AppFooter)

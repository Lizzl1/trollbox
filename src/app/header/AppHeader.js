import { memo, useState } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import FinanceVoteLogo from 'components/logos/FinanceVoteLogo'
import { useManagement } from 'utils/management'
import Button from './button'
import Help from './help.svg'
import './AppHeader.scss'

function AppHeader ({ className, children }) {
  const [ isManagement, setIsManagement ] = useState(true)

  useManagement(({ isManagement }) => {
    setIsManagement(isManagement)
  })

  return (
    <div className={classnames('app-header', className)}>
      <Container className="app-header__container">
        <Row>
          <Col xs="auto" className="text-left">
            <NavLink to="/">
              <FinanceVoteLogo />
            </NavLink>
          </Col>
          <Col className="app-header__menu text-right">
            <nav>
              {isManagement && (
                <Button to="/management">Management</Button>
              )}
              <Button to="/identity">Mint an identity</Button>
              <Button to="/account">Account</Button>
              <Button to="/vote/how-it-works">
                <img src={Help} alt="Help" />
              </Button>
            </nav>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

AppHeader.propTypes = {
  className: PropTypes.string
}

export default memo(AppHeader)

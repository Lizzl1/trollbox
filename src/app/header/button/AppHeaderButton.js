import { memo } from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Button from 'react-bootstrap/Button'
import './AppHeaderButton.scss'

function AppHeaderButton (props) {
  const { className } = props

  return (
    <NavLink
      {...props}
      className={classnames('app-header-button', className)}
      component={Button}
      variant="link"
    />
  )
}

AppHeaderButton.propTypes = {
  className: PropTypes.string
}

export default memo(AppHeaderButton)

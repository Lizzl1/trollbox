import renderer from 'react-test-renderer'
import Account from './Account'

test('should match snapshot', () => {
  const tree = renderer.create(<Account />).toJSON()

  expect(tree).toMatchSnapshot()
})

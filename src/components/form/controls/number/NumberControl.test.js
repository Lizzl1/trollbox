import renderer from 'react-test-renderer'
import NumberControl from './NumberControl'

test('should match snapshot', () => {
  const tree = renderer.create(<NumberControl />).toJSON()

  expect(tree).toMatchSnapshot()
})

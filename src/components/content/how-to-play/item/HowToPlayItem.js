import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './HowToPlayItem.scss'

function HowToPlayItem ({ className, icon, label }) {
  return (
    <div className={classnames('how-to-play__item', className)}>
      <img className="how-to-play__item__icon" src={icon} alt={label} />
      <div className="how-to-play__item__label">{label}</div>
    </div>
  )
}

HowToPlayItem.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.element.isRequired,
  label: PropTypes.string.isRequired
}

export default memo(HowToPlayItem)

import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Card from 'components/containers/card'
import MintForm from './IdentityFormMintForm'
import './IdentityForm.scss'

function IdentityForm ({ className }) {
  return (
    <Card className={classnames('identity-form', className)}>
      <MintForm />
    </Card>
  )
}

IdentityForm.propTypes = {
  className: PropTypes.string
}

export default memo(IdentityForm)

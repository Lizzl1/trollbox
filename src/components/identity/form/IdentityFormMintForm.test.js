import renderer from 'react-test-renderer'
import IdentityFormMintForm from './IdentityFormMintForm'

test('should match snapshot', () => {
  const tree = renderer.create(<IdentityFormMintForm />).toJSON()

  expect(tree).toMatchSnapshot()
})

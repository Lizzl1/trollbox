import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import IdentityForm from './IdentityForm'

test('should match snapshot', () => {
  const tree = renderer.create(
    <MemoryRouter>
      <IdentityForm />
    </MemoryRouter>
  ).toJSON()

  expect(tree).toMatchSnapshot()
})

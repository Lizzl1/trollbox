import renderer from 'react-test-renderer'
import IdentityFormMintFormSuccess from './IdentityFormMintFormSuccess'

test('should match snapshot', () => {
  const tree = renderer.create(<IdentityFormMintFormSuccess />).toJSON()

  expect(tree).toMatchSnapshot()
})

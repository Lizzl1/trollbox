import { Fragment, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Loader from 'components/containers/loader'
import Spinner from 'components/indicators/Spinner'
import { useTokenBalance, useIdentityPrice, useNumIdentities } from 'queries/token'
import { createIdentity } from '../../../utils/simpleSetters'
import Success from './IdentityFormMintFormSuccess'

function IdentityFormMint () {
  const { register, handleSubmit, errors, reset, formState } = useForm()
      , { isSubmitting, isSubmitSuccessful } = formState
      , tokenBalance = useTokenBalance()
      , identityPrice = useIdentityPrice()
      , numidentities = useNumIdentities()
      , [ createdIdentity, setCreatedIdentity ] = useState(0)

  async function onSubmit (args) {
    const { price, referer } = args

    console.log({ args, referer })
    const id = await createIdentity(price.slice(0, -5), parseInt(referer))

    setCreatedIdentity(id)
    return id
  }

  // TODO: is this still necessary? I think useQuery takes care of this now
  useEffect(() => {
    const timeout1 = setTimeout(identityPrice.refetch, 1000)
        , interval1 = setInterval(identityPrice.refetch, 1000)
        , timeout2 = setTimeout(tokenBalance.refetch, 15000)
        , interval2 = setInterval(tokenBalance.refetch, 15000)

    return () => {
      clearTimeout(timeout1)
      clearInterval(interval1)
      clearTimeout(timeout2)
      clearInterval(interval2)
    }
  }, [ identityPrice.refetch, tokenBalance.refetch ])

  return (
    <Form noValidate onSubmit={handleSubmit(onSubmit)}>
      <Loader
        isLoading={createdIdentity.isLoading || isSubmitting}
        error={createdIdentity.error}
        showContentOnError={true}
      >
        {isSubmitSuccessful
          ? (
            <Success
              ownedTokens={createdIdentity}
              mintAnotherCallback={() => reset()}
            />
          )
          : (
            <Fragment>
              {isSubmitting && (
                <Spinner overlay={ true } />
              )}

              <Form.Row>
                <Col>
                  <Form.Label>Identity Available</Form.Label>
                  <Form.Control
                    name="futureIdentity"
                    value={`FVT ${numidentities.data + 1}`}
                    readOnly
                    isInvalid={errors.futureIdentity}
                    ref={register({ required: true })}
                  />
                  <Form.Control.Feedback type={errors.futureIdentity ? 'invalid' : 'valid'}>
                    {errors.futureIdentity?.message}
                  </Form.Control.Feedback>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col>
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    name="price"
                    value={`${identityPrice.data} $FVT`}
                    readOnly
                    isInvalid={errors.price}
                    ref={register({ required: true })}
                  />
                  <Form.Control.Feedback type={errors.price ? 'invalid' : 'valid'}>
                    {errors.price?.message}
                  </Form.Control.Feedback>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col>
                  <Form.Label>$FVT Balance</Form.Label>
                  <Loader
                    isLoading={tokenBalance.isLoading}
                    error={tokenBalance.error}
                  >
                    <Form.Control
                      name="tokenBalance"
                      value={tokenBalance.data}
                      readOnly
                      isInvalid={errors.tokenBalance}
                      ref={register({ required: true })}
                    />
                  </Loader>
                  <Form.Control.Feedback type={errors.tokenBalance ? 'invalid' : 'valid'}>
                    {errors.tokenBalance?.message}
                  </Form.Control.Feedback>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col>
                  <Form.Label>Identity That Referred You</Form.Label>
                  <Form.Control
                    name="referer"
                    defaultValue={0}
                    isInvalid={errors.referer}
                    ref={register({ required: false })}
                  />
                  <Form.Control.Feedback type={errors.referer ? 'invalid' : 'valid'}>
                    {errors.referer?.message}
                  </Form.Control.Feedback>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col>
                  <Button
                    type="submit"
                    variant="secondary"
                    disabled={isSubmitting}
                  >
                    Mint identity
                  </Button>
                </Col>
              </Form.Row>
              {createdIdentity.error && (
                <Form.Row>
                  <Form.Control.Feedback type="invalid">
                    {createdIdentity.error.toString()}
                  </Form.Control.Feedback>
                </Form.Row>
              )}
            </Fragment>
          )
        }
      </Loader>
    </Form>
  )
}

export default IdentityFormMint

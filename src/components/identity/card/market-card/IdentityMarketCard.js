import { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import { formatNumber } from 'utils/formats'
import './IdentityMarketCard.scss'
import { claimPowerOneRound } from 'utils/simpleSetters'

function IdentityMarketCard ({ className, name, rewards, showBallot, roundId, voterId }) {
  const unclaimedRewards = !rewards.synced && parseInt(rewards.vBonus) > 0
      , claim = () => claimPowerOneRound(voterId, roundId)

  return (
    <Container
      className={classnames('identity-market-card', className)}
    >
      <Row>
        <Col xs={2}>
          <div class="title">Round</div>
          {name}
        </Col>
        <Col>
          <div class="title">{ unclaimedRewards ? 'Rewards to be claimed' : 'Rewards claimed' }</div>
          <Row>
            <Col
              xs="4"
              title={rewards.v}
            >
              {'$V ' + formatNumber(rewards.vBonus)}
            </Col>
            <Col
              xs="7"
              title={rewards.fvtBonus}
            >
              {'$FVT ' + formatNumber(rewards.fvtBonus)}
            </Col>
          </Row>
        </Col>
        <Col xs={3} className="text-right">
          <Button className="identity-market-card__button" variant="primary" onClick={() => showBallot(roundId)}>View</Button>
          <Button className="identity-market-card__button claim" variant="primary" onClick={claim} disabled={!unclaimedRewards}>Claim</Button>
        </Col>
      </Row>
    </Container>
  )
}

IdentityMarketCard.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  rewards: PropTypes.shape({
    vBonus: PropTypes.number,
    fvtBonus: PropTypes.number
  }),
  roundId: PropTypes.number
}

IdentityMarketCard.defaultProps = {
  rewards: {
    vBonus: 0,
    fvtBonus: 0
  }
}

export default memo(IdentityMarketCard)

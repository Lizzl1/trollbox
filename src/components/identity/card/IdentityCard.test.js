import renderer from 'react-test-renderer'
import IdentityCard from './IdentityCard'

test('should match snapshot', () => {
  const tree = renderer.create(<IdentityCard />).toJSON()

  expect(tree).toMatchSnapshot()
})

import { memo, Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import Spinner from 'components/indicators/Spinner'
import './Loader.scss'

function Loader ({ className, isLoading, error, children, showContentOnError }) {
  useEffect(() => error && console.error(error.stack), [ error ])

  if (error) {
    return (
      <Fragment>
        {showContentOnError && children}
        <div className="loader-error text-danger">{error.message || error.toString()}</div>
      </Fragment>
    )
  }

  if (isLoading) {
    return (
      <div className="loader-spinner">
        <Spinner />
      </div>
    )
  }

  return children
}

Loader.propTypes = {
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  error: PropTypes.object,
  children: PropTypes.any,
  showContentOnError: PropTypes.bool
}

export default memo(Loader)

import renderer from 'react-test-renderer'
import Card from './Card'

test('should match snapshot', () => {
  const tree = renderer.create(<Card>Foo</Card>).toJSON()

  expect(tree).toMatchSnapshot()
})

import renderer from 'react-test-renderer'
import BoxedValue from './BoxedValue'

test('should match snapshot', () => {
  const tree = renderer.create(
    <BoxedValue label="My Boxed Value" value="1,000.00" />
  ).toJSON()

  expect(tree).toMatchSnapshot()
})

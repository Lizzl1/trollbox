import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './BoxedValue.scss'

function BoxedValue ({ className, label, value }) {
  return (
    <fieldset className={classnames(
      'boxed-value',
      { 'boxed-value--no-value': !value },
      className
    )}>
      <legend>{label}</legend>
      <div className="boxed-value__value">{value}</div>
    </fieldset>
  )
}

BoxedValue.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ])
}

export default memo(BoxedValue)

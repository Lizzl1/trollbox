import { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import numeral from 'numeral'
import moment from 'moment'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import AssetList from 'components/vote/form/list'
import AssetCard, { AssetCardTypes } from 'components/asset-card'
import PendingIcon from './pending.svg'
import CompleteIcon from './complete.svg'
import './VoteBallot.scss'

function formatDate (date) {
  return moment.unix(date).format('DD/MM/YY')
}

function formatBallotNumber (ballotNumber) {
  return numeral(ballotNumber).format('00')
}

function VoteBallot ({ className, ballotNumber, startDate, endDate, resultsDate, projectedWinner, marketWinner, votes, voteMax, showCompletePendingIcon = true, ballotLabel = 'Community votes', sort = true }) {
  const syntheticVotes = useMemo(() => (
    votes && votes.map((vote, index) => ({
      ...vote,
      maxVotes: voteMax,
      className: classnames(
        'vote-ballot__asset-list__item',
        {
          'vote-ballot__asset-list__item--highlight': marketWinner && marketWinner.name === vote.name
        },
        vote.className
      ),
      showWeight: true
    }))
  ), [ votes, marketWinner, voteMax ])

  return (
    <Container
      className={classnames('vote-ballot', className)}
      fluid
    >
      <Row>
        <Col className="vote-ballot__label">
          {formatDate(startDate)} - {formatDate(resultsDate)}
        </Col>
      </Row>
      <Row>
        <Col>
          <h1>Ballot #{formatBallotNumber(ballotNumber)}</h1>
        </Col>
        {showCompletePendingIcon &&
          <Col className="text-right">
            <img
              className="vote-ballot__icon"
              src={marketWinner ? CompleteIcon : PendingIcon}
              alt={marketWinner ? 'complete' : 'pending'}
            />
          </Col>
        }
      </Row>
      <Row className="vote-ballot__result-cards">
        {projectedWinner &&
          <Col>
            <AssetCard
              type={AssetCardTypes.PROJECTED}
              title="Projected winner"
              asset={ projectedWinner && ({
                icon: projectedWinner.icon,
                name: projectedWinner.name
              })}
            />
          </Col>
        }
        <Col>
          <AssetCard
            type={marketWinner ? AssetCardTypes.COMPLETED : AssetCardTypes.PENDING}
            title="Market winner"
            asset={marketWinner && ({
              icon: marketWinner.icon,
              name: marketWinner.name
            })}
            date={resultsDate}
          />
        </Col>
      </Row>
      <Row>
        <Col className="vote-ballot__label">
          {ballotLabel}
        </Col>
      </Row>
      <Row>
        <Col>
          <AssetList
            className="vote-ballot__asset-list"
            items={syntheticVotes}
            sort={sort}
          />
        </Col>
      </Row>
    </Container>
  )
}

VoteBallot.propTypes = {
  className: PropTypes.string,
  ballotNumber: PropTypes.number.isRequired,
  startDate: PropTypes.number.isRequired,
  endDate: PropTypes.number.isRequired,
  resultsDate: PropTypes.number.isRequired,
  projectedWinner: PropTypes.shape({
    icon: PropTypes.isRequired,
    name: PropTypes.isRequired
  }),
  marketWinner: PropTypes.shape({
    icon: PropTypes.isRequired,
    name: PropTypes.isRequired
  }),
  votes: PropTypes.array.isRequired
}

export default memo(VoteBallot)

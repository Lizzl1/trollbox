import renderer from 'react-test-renderer'
import VoteBallot from './VoteBallot'

test('should match snapshot', () => {
  const tree = renderer.create(<VoteBallot />).toJSON()

  expect(tree).toMatchSnapshot()
})

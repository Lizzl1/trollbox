import renderer from 'react-test-renderer'
import VoteForm from './VoteForm'

test('should match snapshot', () => {
  const tree = renderer.create(<VoteForm />).toJSON()

  expect(tree).toMatchSnapshot()
})

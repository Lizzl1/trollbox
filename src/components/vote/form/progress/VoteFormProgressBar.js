import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import ProgressBar from 'react-bootstrap/ProgressBar'
import './VoteFormProgressBar.scss'

function VoteFormProgressBar (props) {
  const { className, now, min } = props
      , isNegative = (now || 0) < (min || 0)

  return (
    <ProgressBar
      {...props}
      variant={isNegative ? 'danger' : 'primary'}
      className={classnames(
        'vote-form-progress-bar',
        { 'vote-form-progress-bar--negative': isNegative },
        className
      )}
    />
  )
}

VoteFormProgressBar.propTypes = {
  className: PropTypes.string,
  now: PropTypes.number,
  min: PropTypes.number
}

VoteFormProgressBar.defaultProps = {
  now: 0,
  min: 0
}

export default memo(VoteFormProgressBar)

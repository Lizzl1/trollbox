import { memo, useState, useMemo, useCallback, useEffect } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Button from 'react-bootstrap/Button'
import Loader from 'components/containers/loader'
import { useSubmitVote } from 'queries/token'
import ProgressBar from './progress'
import List from './list'
import CancelPrompt from './prompt'
import { identityAlreadyVoted } from '../../../utils/simpleGetters'
import './VoteForm.scss'

function VoteForm ({ className, items, tokensAvailable, selectedIdentity, tournamentId, updateRoundId }) {
  const [ votes, setVotes ] = useState({})
      , [ hasVoted, setHasVoted ] = useState(false)
      , [ cancelPrompt, setCancelPrompt ] = useState(false)
      , [ maxVotes, setMaxVotes ] = useState(0)

      , [ submitVote, submittedVote ] = useSubmitVote()

      , tokensSpent = useMemo(() => (
        Object.values(votes).reduce((accumulator, currentValue) => accumulator + (currentValue ** 2), 0)
      ), [ votes ])

      , tokensLeft = useMemo(() => (
        tokensAvailable - tokensSpent
      ), [ tokensAvailable, tokensSpent ])

      , changeVoteWeight = useCallback((index, weight) => {
        setVotes({ ...votes, [index]: weight })
      }, [ votes ])

      , handleItemChange = useCallback((value, index) => {
        changeVoteWeight(index, parseFloat(value) || 0)
      }, [ changeVoteWeight ])

      , syntheticItems = useMemo(() => (
        items && items.map((item, index) => ({
          ...item,
          weight: votes[index],
          tokensAvailable,
          editable: !!selectedIdentity && !hasVoted,
          maxVotes: maxVotes,
          index,
          onChange: handleItemChange
        }))
      ), [ items, handleItemChange, votes, selectedIdentity, tokensAvailable, hasVoted, maxVotes ])

      , alreadyVoted = useMemo(async () => {
        if (selectedIdentity) {
          const hasVoted = await identityAlreadyVoted(selectedIdentity, tournamentId)

          if (!hasVoted) {
            resetVotes()
          }
          setHasVoted(hasVoted)
        }
      }, [ selectedIdentity, tournamentId ])

  function resetVotes () {
    setVotes(votes => ({
      ...Object
        .keys(votes)
        .reduce((votes, _, index, keys) => (
          { ...votes, [keys[index]]: 0 }
        ), {})
    }))
    setMaxVotes(0)
    submittedVote.reset()
  }

  async function handleSubmit () {
    const choices = []
        , userVotes = []

    Object.entries(votes).forEach(([ choice, vote ]) => {
      if (parseInt(vote) > 0) {
        choices.push(parseInt(choice) + 1)
        userVotes.push(vote)
      }
    })

    const rcpt = await submitVote({ selectedIdentity, tournamentId, choices, userVotes, updateRoundId })
        , complete = !!rcpt

    console.log('complete', complete)
    setHasVoted(complete)
  }

  function handleCancel () {
    setCancelPrompt(true)
  }

  function handleHideCancelPrompt () {
    setCancelPrompt(false)
  }

  function handleExit () {
    setCancelPrompt(false)
    resetVotes()
  }

  useEffect(() => {
    resetVotes()
  }, [ selectedIdentity ])

  return (
    <div className={classnames('vote-form', className)}>
      <Loader
        isLoading={submittedVote.isLoading}
        error={submittedVote.error}
        showContentOnError={true}
      >
        {cancelPrompt && (
          <CancelPrompt
            show={true}
            onClose={handleHideCancelPrompt}
            onExit={handleExit}
          />
        )}
        {hasVoted
          ? (
            <div
              className="vote-form__success-message"
            >
              <strong>Your vote has been submitted</strong>
              <br />
              Check back in on the Payout date to see if your
              predictions were correct and collect your winnings!
            </div>
          )
          : (
            <div className="vote-form__progress-bar">
              <div className="vote-form__progress-bar__label">
                {selectedIdentity
                  ? (
                    <span>{tokensLeft} $V Left</span>
                  )
                  : (
                    <span>Connect your Identity to start voting</span>
                  )
                }
              </div>
              <ProgressBar now={tokensLeft} max={tokensAvailable} />
            </div>
          )
        }
        <List
          items={syntheticItems}
          sort={hasVoted}
        />
        {(!hasVoted && (submittedVote.isIdle || submittedVote.error)) && (
          <div className="vote-form__actions">
            <Button
              className="vote-form__submit-button"
              variant="secondary"
              disabled={tokensSpent <= 0 || tokensLeft < 0}
              onClick={handleSubmit}
            >
              Vote
            </Button>
            <div className="vote-form__footer">
              {tokensLeft < 0 && (
                <div className="text-danger">
                  Over allocated, please adjust your votes to
                  get your $FVT left as close to 0 as possible
                </div>
              )}
              {tokensSpent > 0 && (
                <Button
                  className="vote-form__cancel-button"
                  variant="link"
                  onClick={handleCancel}
                >
                  Cancel vote
                </Button>
              )}
            </div>
          </div>
        )}
      </Loader>
    </div>
  )
}

VoteForm.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  tokensAvailable: PropTypes.number,
  selectedIdentity: PropTypes.string,
  tournamentId: PropTypes.number
}

VoteForm.defaultProps = {
  tokensAvailable: 0
}

export default memo(VoteForm)

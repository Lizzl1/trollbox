import renderer from 'react-test-renderer'
import VoteFormAssetList from './VoteFormAssetList'

test('should match snapshot', () => {
  const tree = renderer.create(<VoteFormAssetList />).toJSON()

  expect(tree).toMatchSnapshot()
})

import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import List from 'components/asset-list'
import Item from './item'
import './VoteFormAssetList.scss'

function VoteFormAssetList (props) {
  const { sort } = props

  return (
    <List
      {...props}
      className={classnames('vote-form-asset-list', props.className)}
      itemComponent={Item}
      sort={sort
        ? (item1, item2) => {
          const weight1 = item1.weight
              , weight2 = item2.weight

          if (weight1 < weight2) {
            return 1
          }

          if (weight1 > weight2) {
            return -1
          }

          return 0
        }
        : () => 0
      }
    />
  )
}

VoteFormAssetList.propTypes = {
  className: PropTypes.string,
  sort: PropTypes.bool
}

export default memo(VoteFormAssetList)

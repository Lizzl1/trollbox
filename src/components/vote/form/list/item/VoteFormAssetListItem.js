import { memo, useMemo, useCallback } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Col from 'react-bootstrap/Col'
import Item from 'components/asset-list/item'
import NumberControl from 'components/form/controls/number'
import './VoteFormAssetListItem.scss'

function VoteFormAssetListItem (props) {
  const { className, weight, tokensAvailable, maxVotes, index, editable, onChange } = props
      , itemProps = { ...props }

      , maxValue = useMemo(() => (
        maxVotes || Math.ceil(Math.sqrt(tokensAvailable)) || Infinity
      ), [ maxVotes, tokensAvailable ])

      , handleWeightChange = useCallback(value => (
        onChange && onChange(value, index)
      ), [ index, onChange ])

  delete itemProps.weight
  delete itemProps.editable
  delete itemProps.onChange

  return (
    <Item
      {...props}
      progress={(weight * 100) / maxValue}
      className={classnames('vote-form-asset-list-item', className)}
    >
      <Col xs="auto">
        <fieldset className={classnames('vote-form-asset-list-item__weight', editable ? 'editable' : '')}>
          <legend>$Vote</legend>
          <NumberControl
            min={0}
            max={maxValue}
            value={weight}
            readOnly={!editable}
            placeholder="0"
            onChange={handleWeightChange}
          />
        </fieldset>
      </Col>
    </Item>
  )
}

VoteFormAssetListItem.propTypes = {
  className: PropTypes.string,
  weight: PropTypes.number,
  tokensAvailable: PropTypes.number,
  index: PropTypes.number,
  showWeight: PropTypes.bool,
  editable: PropTypes.bool,
  onChange: PropTypes.func
}

export default memo(VoteFormAssetListItem)

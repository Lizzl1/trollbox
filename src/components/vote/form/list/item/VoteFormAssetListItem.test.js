import renderer from 'react-test-renderer'
import VoteFormAssetListItem from './VoteFormAssetListItem'

test('should match snapshot', () => {
  const tree = renderer.create(
    <VoteFormAssetListItem
      icon=""
      name="Asset"
      symbol="ASST"
      price={{
        symbol: '$',
        value: 100,
        difference: 0.42
      }}
    />
  ).toJSON()

  expect(tree).toMatchSnapshot()
})

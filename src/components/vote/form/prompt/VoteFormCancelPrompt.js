import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import './VoteFormCancelPrompt.scss'

function VoteFormCancelPrompt ({ className, show, onClose, onExit }) {
  return (
    <Modal
      className={classnames('vote-form-cancel-prompt', className)}
      show={show}
      centered
      onHide={onClose}
    >
      <Modal.Header closeButton />
      <Modal.Body>
        <legend>Cancel vote</legend>
        <h2>Are you sure you want to exit the vote?</h2>
        <p>
          By clicking exit your vote will be discarded and
          your tokens will not be collected
        </p>
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center">
        <Button variant="secondary" onClick={onClose}>Resume</Button>
        <Button variant="danger" onClick={onExit}>Exit</Button>
      </Modal.Footer>
    </Modal>
  )
}

VoteFormCancelPrompt.propTypes = {
  className: PropTypes.string,
  show: PropTypes.bool,
  onClose: PropTypes.func,
  onExit: PropTypes.func
}

export default memo(VoteFormCancelPrompt)

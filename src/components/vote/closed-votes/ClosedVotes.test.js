import renderer from 'react-test-renderer'
import ClosedVotes from './ClosedVotes'

test('should match snapshot', () => {
  const tree = renderer.create(<ClosedVotes />).toJSON()

  expect(tree).toMatchSnapshot()
})

import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { NavLink } from 'react-router-dom'
import './NavLinkButton.scss'

function NavLinkButton ({ className, to, children }) {
  return (
    <div className={classnames('nav-link-button', className)}>
      <NavLink
        activeClassName="active"
        to={to}
      >
        {children}
      </NavLink>
    </div>
  )
}

NavLinkButton.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ])
}

export default memo(NavLinkButton)

import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import NavLinkButton from './NavLinkButton'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <NavLinkButton to="/test">My nav link button</NavLinkButton>
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

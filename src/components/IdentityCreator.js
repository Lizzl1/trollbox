import { useState } from 'react'
import { Input, Button } from 'antd'
import {
  createIdentityFor
} from 'utils/simpleSetters'

function IdentitySelector () {
  const [ targetAddress, setTargetAddress ] = useState('')

  return (
    <div>
      <Input onChange={e => setTargetAddress(e.target.value)}/>
      <Button type="primary" onClick={() => { createIdentityFor(targetAddress) }} > Create Identity For </Button>
    </div>
  )
}

export default IdentitySelector

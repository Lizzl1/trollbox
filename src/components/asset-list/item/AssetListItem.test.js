import renderer from 'react-test-renderer'
import AssetListItem from './AssetListItem'

test('should match snapshot', () => {
  const tree = renderer.create((
    <AssetListItem
      icon=""
      name="Asset"
      symbol="ASST"
      price={{
        symbol: '$',
        value: 100,
        difference: 0.42
      }}
    />
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

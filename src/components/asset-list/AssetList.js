import { memo, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { Scrollbars } from 'react-custom-scrollbars'
import { AutoSizer } from 'react-virtualized'
import Item from './item'
import './AssetList.scss'

const ScrollbarView = props => (<div {...props} className="asset-list__scrollbars__view" />)
    , ScrollbarThumb = props => (<div {...props} className="asset-list__scrollbars__thumb" />)

function AssetList ({ className, items, itemComponent, sort }) {
  const ItemComponent = itemComponent
      , hasItems = items && items.length > 0

  return (
    <div className={classnames(
      'asset-list',
      { 'asset-list--empty': !hasItems },
      className
    )}>
      {hasItems
        ? (
          <AutoSizer>
            {({ width, height }) => (
              <Scrollbars
                style={{ width, height }}
                renderView={ScrollbarView}
                renderThumbHorizontal={() => (<div style={{ display: 'none' }}></div>)}
                renderThumbVertical={ScrollbarThumb}
              >
                {items.sort(sort || (() => 0)).map((item) => (
                  <ItemComponent
                    key={`item${item.symbol}`}
                    {...item}
                    className={classnames('asset-list__item', item.className)}
                  />
                ))}
              </Scrollbars>
            )}
          </AutoSizer>
        )
        : (
          <Fragment>List is empty</Fragment>
        )
      }
    </div>
  )
}

AssetList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    symbol: PropTypes.string.isRequired,
    price: PropTypes.shape({
      symbol: PropTypes.string.isRequired,
      value: PropTypes.number.isRequired,
      difference: PropTypes.number.isRequired
    }).isRequired
  })),
  itemComponent: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.object
  ]),
  sort: PropTypes.func
}

AssetList.defaultProps = {
  itemComponent: Item
}

export default memo(AssetList)

import {
  bytesToString,
  getContract,
  isUserManagement,
  getEthAccount,
  getAllEvents,
  toWei,
  fromWei,
  sha3,
  toBN
} from './ethereum.js'

import {
  getWinner,
  getOwner,
  getClaimablePowerAll,
  getClaimablePowerRound,
  getUnclaimedTokens,
  getVoiceCredits,
  getTokenList
} from './simpleGetters'

import { getChainlinkDecimals } from './price'

import Trollbox from 'truffle/build/contracts/Trollbox.json'
import TrollboxProxy from 'truffle/build/contracts/TrollboxProxy.json'
import FVT from 'truffle/build/contracts/Token.json'
import Identity from 'truffle/build/contracts/Identity.json'
import ReferralProgram from 'truffle/build/contracts/ReferralProgram.json'
import ChainLinkOracle from 'truffle/build/contracts/ChainLinkOracle.json'
import tokenListJSON from 'tokenList.json'

const transferEventSignature = sha3('Transfer(address,address,uint256)')
    , voteEventSignature = sha3('VoteOccurred(uint256,uint256,uint256,uint256[],uint256[],bytes32)')
    , winnerConfirmedEventSignature = sha3('WinnerConfirmed(uint256,uint256,int[])')

export async function getVotesByIdentity (callback, identityNum, tournamentId = 1) {
  console.log('getVotesByIdentity')
  const filter = { tournamentId: tournamentId, voterId: identityNum }

  await getAllEvents(
    async (event) => {
      console.log('VoteOccurred event', event)
      const winner = await getWinner(tournamentId, event.roundId)

      console.log('VoteOccurred event', event)
      event.winner = winner.toString()

      const winnings = await getClaimablePowerRound(identityNum, tournamentId, event.roundId)

      winnings.fvtBonus = parseFloat(fromWei(winnings.fvtBonus))
      event.winnings = winnings
      callback(event)
    }, Trollbox, 'VoteOccurred', filter, 0, 'latest', voteEventSignature, Trollbox.abi[9].inputs)
}

export async function getIdentitiesForAccount (callback) {
  console.log('getIdentitiesForAccount')
  const account = await getEthAccount()
      , filter = { to: account }
      // , identity = await getContract('read', Identity)

  await getAllEvents(
    async (event) => {
      const owner = await getOwner(event.tokenId)

      console.log('event', event)
      if (owner.toLowerCase() === account.toLowerCase()) {
        const pow = await getClaimablePowerAll(event.tokenId, 1)
            , unclaimedTokens = await getUnclaimedTokens(event.tokenId)
            , currentV = await getVoiceCredits(1, event.tokenId)

        event.unclaimedRounds = pow.unclaimedRounds
        event.unclaimedTokens = unclaimedTokens
        console.log('event.unclaimedTokens', event.unclaimedTokens)
        event.fvtBalance = unclaimedTokens
        event.voteMarkets = [
          {
            name: 'DeFi Winners',
            v: currentV,
            rewards: {
              v: pow.vBonus,
              fvt: pow.fvtBonus
            }
          }
        ]
        console.log('pow', pow)
        callback(event)
      } else {
        console.log('owner', owner, 'account', account)
      }
    }, Identity,
    'Transfer', filter,
    0, 'latest',
    transferEventSignature, Identity.abi[8].inputs
  )
}

export async function getHistoricChainlinkPrices (roundId, callback) {
  // console.log('getHistoricPricesForAccount', roundId)
  const filter = { roundId }
      , tokens = await getTokenList('')

  await getAllEvents(
    async (event) => {
      // console.log('historic price event', event)
      const prices = {}

      for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i]
        prices[token.symbol] = {
          value: event.prices[i] / (10 ** (await getChainlinkDecimals(token.symbol))),
          difference: 0
        }
      }
      // console.log('processed prices', { prices })
      callback(prices)
    }, ChainLinkOracle, 'WinnerConfirmed', filter, 0, 'latest',
    winnerConfirmedEventSignature, ChainLinkOracle.abi[9].inputs
  )
}

export async function getHistoricChainlinkPricesWrapper (roundId) {
  return new Promise((resolve) => {
    getHistoricChainlinkPrices(roundId, (successResponse) => {
      resolve(successResponse)
    })
  })
}

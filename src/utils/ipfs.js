const ipfsClient = require('ipfs-http-client')
    , multihashes = require('multihashes')
    , { REACT_APP_ETHERSCAN_STEM, REACT_APP_IPFS_GATEWAY_URL } = process.env
    , ipfs = ipfsClient('ipfs.infura.io', '5001', { protocol: 'https' })

// export {ipfs};

export function getIpfsHashFromEthBytes (ethBytes) {
  return multihashes.toB58String(multihashes.fromHexString('1220' + ethBytes.slice(2)))
}

export function getEthBytesFromIpfsHash (ipfsHash) {
  return '0x' + multihashes.fromB58String(ipfsHash).toString('hex').slice(4)
}

export function getIpfsLinkFromEthBytes (ethBytes) {
  return REACT_APP_IPFS_GATEWAY_URL + getIpfsHashFromEthBytes(ethBytes)
}

export function getEtherscanLinkFromLoan (loan) {
  return REACT_APP_ETHERSCAN_STEM + loan.txHash
}

export async function getIPFSFromBytes (bytes) {
  const hash = getIpfsHashFromEthBytes(bytes)

  return await getIPFS(hash)
}

export async function getIPFS (hash) {
  const MAX_LENGTH = 10000
      , OFFSET = 0

  async function ipfsCat (hash) {
    try {
      console.log('Getting ipfs hash ', hash)
      const file = await ipfs.cat(hash, OFFSET, MAX_LENGTH)
          , json = JSON.parse(file.toString('utf8'))

      console.log('Got hash', hash, json)
      return json
    } catch (err) {
      console.error('err', err)
      if (err.name === 'TimeoutError') {
        console.warn('IPFS download timed out, trying again')
        return await ipfsCat(hash)
      } else {
        console.error(err)
        alert(`File failed to download from IPFS, please try again: ${err}`)
      }
    }
  }
  return await ipfsCat(hash)
}

export async function addIPFS (json) {
  const opts = {
        pin: true,
        recursive: false
      }
      , buff = Buffer.from(JSON.stringify(json))

  async function ipfsAdd (buff, opts) {
    try {
      const hash = await ipfs.add(buff, opts)
          , msg = `Successfully added to ipfs ${hash[0].path}`

      console.log(msg)
      return hash[0].path
    } catch (err) {
      console.error(err)
      if (err.name === 'TimeoutError') {
        console.warn('IPFS upload timed out, trying again')
        return await ipfsAdd(buff, opts)
      } else {
        console.error(err)
        alert(`File failed to upload to IPFS, please try again: ${err}`)
      }
    }
  }
  return await ipfsAdd(buff, opts)
}

// export async function addFileIPFS(file) {
//   const opts = {
//     pin: true,
//     recursive: true
//   };
//   async function ipfsAdd(file, opts) {
//     try {
//       let hash = await ipfs.add(file, opts);
//       const msg = `File uploaded to IPFS successfully: ${hash[0].path}`;
//       console.log(msg);
//       alert(msg)
//       return hash;
//     } catch (err) {
//       console.error(err);
//       if (err.name === 'TimeoutError') {
//         console.warn('IPFS upload timed out, trying again');
//         return await ipfsAdd(file, opts);
//       } else {
//         console.error(err);
//         alert(`File failed to upload to IPFS, please try again: ${err}`);
//       }
//     }
//   }
//   return await ipfsAdd(file, opts);
// }

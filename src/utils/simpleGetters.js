import {
  bytesToString,
  getContract,
  isUserManagement,
  getEthAccount,
  getAllEvents,
  toWei,
  fromWei,
  sha3,
  toBN
} from './ethereum.js'

import { getPrices } from './price'
import logos from 'tests/mocks/asset-list'

import Trollbox from 'truffle/build/contracts/Trollbox.json'
import FVT from 'truffle/build/contracts/Token.json'
import Identity from 'truffle/build/contracts/Identity.json'
import ChainLinkOracle from 'truffle/build/contracts/ChainLinkOracle.json'
import tokenListJSON from 'tokenList.json'

const winnerOverrides = {
  14: 8
}

export async function getTokenList (ensAddr) {
  // TODO: figure out how to do this on local
  return tokenListJSON.tokens

//    if (!ENS) {
//        const { ens } = await setupENS()
//        ENS = ens;
//    }
//    const content = await ENS.getContent(ensAddr)
//    const parsed = content.value.slice(7)
//    console.log('content', parsed)
//    // chop off the ipfs://
//    const list = await getIPFS(parsed)
//    return list.tokens
}

export async function getManagement () {
  return {
    trollbox: await isUserManagement(Trollbox),
    identity: await isUserManagement(Identity)
  }
}

export async function isWinnerOracle (tournamentId) {
  const t = await getTournament(tournamentId)
      , account = await getEthAccount()

  return account.toLowerCase() === t.winnerOracle.toLowerCase()
}

export async function getTokenBalance () {
  const account = await getEthAccount()
      , fvt = await getContract('read', FVT)
      , balance = await fvt.balanceOf(account)

  console.log('account', account, 'fvt', fvt, 'balance', balance.toString())
  return fromWei(balance)
}

export async function identityAlreadyVoted (voterId, tournamentId) {
  const trollbox = await getContract('read', Trollbox)
      , currentRound = await trollbox.getCurrentRoundId(tournamentId)
      , lastRoundVoted = await trollbox.getLastRoundVoted(tournamentId, voterId)

  return lastRoundVoted.eq(currentRound)
}

export async function getRoundResolved (tournamentId, roundId) {
  const winner = await getWinner(tournamentId, roundId)

  return winner > 0
}

export async function getWinner (tournamentId, roundId) {
  let winner

  try {
    const trollbox = await getContract('read', Trollbox)
        , round = await trollbox.getRound(tournamentId, roundId)

    winner = round[1]
  } catch (err) {
    console.error(err)
  }

  return winner
}

export async function getLastRoundReward (tournamentId, voterId) {
  const trollbox = await getContract('read', Trollbox)

  trollbox.getLastRoundReward(tournamentId, voterId)
}

export async function getNumIdentities () {
  const identity = await getContract('read', Identity)
      , numIdentities = await identity.numIdentities()

  return await numIdentities.toNumber()
}

export async function roundAlreadyResolved (tournamentId, roundId) {
  const trollbox = await getContract('read', Trollbox)

  return await trollbox.roundAlreadyResolved(tournamentId, roundId)
}

export async function getRoundResolution (tournamentId) {
  const currentRoundId = await getCurrentRoundId(tournamentId)
      , resolution = {}

  for (let roundId = 1; roundId < currentRoundId - 1; roundId++) {
    resolution[roundId] = await roundAlreadyResolved(tournamentId, roundId)
  }
  return resolution
}

export async function isSynced (voterId, tournamentId, roundId) {
  const trollbox = await getContract('read', Trollbox)

  return await trollbox.isSynced(voterId, tournamentId, roundId)
}

export async function getRoundBonus (voterId, tournamentId, roundId) {
  const trollbox = await getContract('read', Trollbox)
      , bonus = await trollbox.getRoundBonus(voterId, tournamentId, roundId)

  return bonus
}

export async function getVoiceCredits (tournamentId, user) {
  const contract = await getContract('read', Trollbox)
      , v = (await contract.getVoiceCredits(tournamentId, user)).toNumber()

  if (v === 0) {
    return await getVoiceUBI(tournamentId)
  }
  return v
}

export async function getVoiceUBI (tournamentId) {
  const contract = await getContract('read', Trollbox)

  return await contract.getVoiceUBI(tournamentId)
}

export async function getClaimablePowerRound (voterId, tournamentId, roundId) {
  const synced = await isSynced(voterId, tournamentId, roundId)
      , bonus = await getRoundBonus(voterId, tournamentId, roundId)

  console.log('roundId', roundId, 'synced', synced, 'bonus', bonus[0].toString(), fromWei(bonus[1]).toString())
  return {
    synced,
    roundId,
    vBonus: bonus[0].toNumber(),
    fvtBonus: bonus[1]
  }
}

export async function getClaimablePowerAll (voterId, tournamentId) {
  const currentRoundId = await getCurrentRoundId(tournamentId)
      , lastClaimableRound = currentRoundId - 2
      , unclaimedRounds = []
      , deferred = []

  let vBonus = 0
    , fvtBonus = toBN(0)

  // TODO: this is linear in the number of rounds...
  for (let roundId = 1; roundId <= lastClaimableRound; roundId++) {
    deferred.push(getClaimablePowerRound(voterId, tournamentId, roundId))
  }

  const results = await Promise.all(deferred)

  results.forEach(result => {
    if (result.fvtBonus.toString() !== '0' && !result.synced) {
      vBonus += result.vBonus
      fvtBonus = fvtBonus.add(result.fvtBonus)
      unclaimedRounds.push(result.roundId)
    }
  })

  fvtBonus = parseFloat(fromWei(fvtBonus))

  return {
    unclaimedRounds,
    vBonus,
    fvtBonus
  }
}

export async function getOwner (token) {
  const id = await getContract('read', Identity)

  return await id.ownerOf(token)
}

export async function getUnclaimedTokens (voterId) {
  const trollbox = await getContract('read', Trollbox)
      , tokensWon = await trollbox.tokensWon(voterId)

  console.log('tokensWon', voterId, tokensWon, fromWei(tokensWon))
  return parseFloat(fromWei(tokensWon))
}

export async function getTournament (tournamentId) {
  const contract = await getContract('read', Trollbox)
      , tournament = await contract.tournaments(tournamentId)

  tournament.startTime = tournament.startTime.toNumber()
  tournament.roundLengthSeconds = tournament.roundLengthSeconds.toNumber()
  tournament.tournamentId = tournament.tournamentId.toNumber()
  tournament.currentRoundId = await getCurrentRoundId(tournamentId)
  tournament.tokenList = await getTokenList('') // TODO: hook up to ENS
  tournament.tokenRoundBonus = fromWei(tournament.tokenRoundBonus)

  return tournament
}

export async function getVoteTotals (tournamentId, roundId, numOptions) {
  // console.log('getVoteTotals', tournamentId, roundId, numOptions)
  const contract = await getContract('read', Trollbox)
      , deferred = []

  for (let i = 1; i <= numOptions; i++) {
    deferred.push(contract.getVoteTotals(tournamentId, roundId, i))
  }

  const votes = await Promise.all(deferred)
      , voteNums = votes.map(vote => vote.toNumber())

  // console.log('voteNums', tournamentId, roundId, voteNums)
  return voteNums
}

export async function getVoteTotal (tournamentId, roundId, option) {
  const contract = await getContract('read', Trollbox)

  return await contract.getVoteTotals(tournamentId, roundId, option)
}

export async function getCurrentRoundId (tournamentId) {
  const contract = await getContract('read', Trollbox)
      , roundId = await contract.getCurrentRoundId(tournamentId)

  return roundId.toNumber()
}

export async function getIdentityPrice () {
  const contract = await getContract('read', Identity)
      , bigPrice = await contract.getIdentityPrice()

  return fromWei(bigPrice)
}

export function getDaysBack (tournament, roundId) {
  if (roundId === 0 || roundId === tournament.currentRoundId - 1) {
    console.log({ tournament })
    const roundStart = tournament.startTime + ((tournament.currentRoundId - 1) * tournament.roundLengthSeconds)
    const now = Math.floor((new Date()).getTime() / 1000)

    return Math.floor((now - roundStart) / 86400)
  } else {
    return 7
  }
}

export async function getWinnerIndex (tournament, roundId) {
  if (roundId === 0 || roundId === tournament.currentRoundId - 1) {
    return 0
  } else {
    return await getWinner(tournament.tournamentId, roundId)
  }
}

export async function getWinnerChainlink () {
  const contract = await getContract('read', ChainLinkOracle)
  const prices = await contract.getWinner()

  console.log('prices', prices)
  return prices
}

export async function getBallotForRound (tournament, roundId) {
  console.log('getBallotForRound', roundId)
  const daysBack = getDaysBack(tournament, roundId)
  const totals = await getVoteTotals(tournament.tournamentId, roundId, tournament.tokenList.length)
  console.log({ totals })
  const voteSum = totals.reduce((x, y) => x + y)
  const winner = winnerOverrides[roundId] ? winnerOverrides[roundId] : await getWinnerIndex(tournament, roundId)
  const voteMax = Math.max(...totals)
  const resolved = await roundAlreadyResolved(tournament.tournamentId, roundId)
  const live = roundId === 0 || roundId === tournament.currentRoundId - 1 || !resolved
  const priceKey = live ? 'live' : roundId
  const prices = await getPrices(priceKey, tournament.tokenList)
  const votes = totals.map((vote, index) => ({
    weight: vote,
    icon: logos[tournament.tokenList[index].symbol.toLowerCase()],
    name: tournament.tokenList[index].name,
    symbol: tournament.tokenList[index].symbol,
    daysBack,
    price: {
      symbol: '$',
      value: prices[tournament.tokenList[index].symbol].value,
      difference: prices[tournament.tokenList[index].symbol].difference
    }
  }))
  const projectedWin = totals.findIndex(x => x === voteMax)
  const projectedWinner = tournament.tokenList[projectedWin]
  const marketWinner = tournament.tokenList[winner - 1]

  return {
    number: roundId,
    startDate: tournament.startTime + ((roundId - 1) * tournament.roundLengthSeconds),
    endDate: tournament.startTime + (roundId * tournament.roundLengthSeconds),
    resultsDate: tournament.startTime + ((roundId + 1) * tournament.roundLengthSeconds),
    projectedWinner: projectedWinner && {
      icon: logos[projectedWinner?.symbol.toLowerCase()],
      name: projectedWinner?.name
    },
    marketWinner: marketWinner && {
      icon: logos[marketWinner?.symbol.toLowerCase()],
      name: marketWinner?.name
    },
    votes,
    voteSum,
    voteMax
  }
}

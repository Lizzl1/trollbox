import moment from 'moment'
import numeral from 'numeral'

export const formatDate = date => moment(date).format('MMM Do, YYYY')

export const formatDateShort = date => moment(date).format('DD/MM/YY')

export const formatNumber = number => {
  const formattedNumber = numeral(number).format('0.[00]')

  if (Math.floor(number) !== number && (number.toString().split('.')[1].length || 0) > 2) {
    return `${formattedNumber}...`
  }

  return formattedNumber
}

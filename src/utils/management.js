import { useState, useEffect, useCallback } from 'react'
import { getManagement } from './simpleGetters'

export const useManagement = onUpdate => {
  const [ isManagement, setIsManagement ] = useState(false)

      , updateUserIsManagement = useCallback(async () => {
        try {
          const { trollbox } = await getManagement()
              , isManagement = trollbox

          setIsManagement(trollbox)
          onUpdate && onUpdate({ isManagement })
        } catch (err) {
          console.error(err)
        }
      }, [ onUpdate ])

  useEffect(() => {
    updateUserIsManagement()
  }, [ updateUserIsManagement ])

  return {
    isManagement
  }
}

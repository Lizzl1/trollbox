import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'

import VotePage from './VotePage'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <VotePage />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

import { memo, useState, useEffect, useMemo, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { NavLink } from 'react-router-dom'
import Button from 'react-bootstrap/Button'
import Card from 'components/containers/card'
import ClosedVotes from 'components/vote/closed-votes'
import Spinner from 'components/indicators/Spinner'
import Page from 'pages/Page'
import { useClosedVoteBallots, useCurrentRoundId } from 'queries/token'
import './HomePage.scss'

function HomePage ({ className, children }) {
  const [ tournamentId ] = useState(1)
  const currentRoundId = useCurrentRoundId(tournamentId)
      // , currentRoundId = { data: 12 }
      , numBallots = 4
      , previousBallotsIds = []

  for (let i = 1; i <= numBallots; i++) {
    previousBallotsIds.push(currentRoundId.data ? currentRoundId.data - i : NaN)
  }

  const previousBallots = useClosedVoteBallots(tournamentId, previousBallotsIds)

  const ballots = useMemo(() => previousBallots.data, [ previousBallots.data ])

  useEffect(() => {
    if (previousBallots.error) {
      console.error(previousBallots.error)
    }
  }, [ previousBallots.error ])

  return (
    <Page
      className={classnames('home-page', className)}
      title="The cryptoeconomic game that marries governance with the markets"
      howToPlay={false}
      leftContent={(
        <Fragment>
          <p>
            Make market predictions in weekly tournaments
            voting on assets from across the cryptospace.
            Win a share of the weekly reward pool and gain
            voting power for the next tournament.
          </p>
          <NavLink
            className="home-page"
            to="/identity"
            variant="primary"
            component={Button}
          >
            Mint an Identity to vote
          </NavLink>
        </Fragment>
      )}
      rightContent={(
        <Card className="home-page__identity-form">
          <p>
            <strong>
              Vote for this weeks winner and
              win a share of the reward pool.
            </strong>
          </p>
          <p>
            Vote correctly and gain more voting power
            for the next round!
          </p>
          <NavLink
            to="/vote"
            variant="secondary"
            component={Button}
          >
            Start voting
          </NavLink>
          <p>
            To enter the tournament you must have
            a finance.vote identity
          </p>
        </Card>
      )}
      bottomContent={(
        <div>
          { previousBallots.isLoading
            ? (
              <Spinner />
            )
            : (
              <ClosedVotes ballots={ballots} />
            )
          }
        </div>
      )}
    />
  )
}

HomePage.propTypes = {
  className: PropTypes.string
}

export default memo(HomePage)

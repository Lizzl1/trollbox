import renderer from 'react-test-renderer'
import Page from './Page'

test('should match snapshot', () => {
  const tree = renderer.create(<Page />).toJSON()

  expect(tree).toMatchSnapshot()
})

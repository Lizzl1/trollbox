import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import AccountPage from './AccountPage'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <AccountPage />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

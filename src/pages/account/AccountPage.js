import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Account from 'components/account'
import Page from 'pages/Page'
import './AccountPage.scss'

function AccountPage ({ className, children }) {
  return (
    <Page
      className={classnames('account-page', className)}
      fullWidth={true}
      howToPlay={false}
      leftContent={(
        <Account />
      )}
    />
  )
}

AccountPage.propTypes = {
  className: PropTypes.string
}

export default memo(AccountPage)

import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import HowToPlay from 'components/content/how-to-play'
import ErrorBoundry from 'utils/ErrorBoundry'
import './Page.scss'

function Page ({ className, fluid, fluidTitle, nav, title, leftContent, rightContent, bottomContent, fullWidth, howToPlay }) {
  const Title = () => (
    <h1 className="page__title">{title}</h1>
  )

  return (
    <Container className={classnames('page', className)} fluid={fluid}>
      <ErrorBoundry>
        {nav && (
          <nav className="page__nav">{nav}</nav>
        )}
        <Row>
          <Col xs={12} lg={fullWidth ? 12 : 6}>
            {title && (
              fluidTitle
                ? (
                  <Title />
                )
                : (
                  <Container>
                    <Row>
                      <Col>
                        <Title />
                      </Col>
                    </Row>
                  </Container>
                )
            )}
            {leftContent && (
              <div className="page__left-content">{leftContent}</div>
            )}
          </Col>
          {rightContent && (
            <Col className="page__right-content text-right">
              {rightContent}
            </Col>
          )}
        </Row>
        {bottomContent && (
          <Row>
            <Col className="page__bottom-content">
              {bottomContent}
            </Col>
          </Row>
        )}
        {howToPlay && (
          <Row>
            <Col lg={10}>
              <HowToPlay />
            </Col>
          </Row>
        )}
      </ErrorBoundry>
    </Container>
  )
}

Page.propTypes = {
  className: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ]),
  leftContent: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ]),
  rightContent: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  bottomContent: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  fullWidth: PropTypes.bool,
  howToPlay: PropTypes.bool,
  fluid: PropTypes.bool,
  fluidTitle: PropTypes.bool,
  nav: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ])
}

Page.defaultProps = {
  howToPlay: true,
  fluidTitle: true
}

export default memo(Page)

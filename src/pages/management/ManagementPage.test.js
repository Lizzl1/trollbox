import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import ManagementPage from './ManagementPage'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <ManagementPage />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

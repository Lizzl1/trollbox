import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import IdentityPage from './IdentityPage'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <IdentityPage />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})

import json
from web3 import Web3
import os
from dotenv import load_dotenv
import pandas as pd

load_dotenv(dotenv_path='.env', verbose=True)
INFURA_API_KEY = os.getenv("INFURA_API_KEY")

w3 = Web3(Web3.WebsocketProvider('wss://mainnet.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
print('Current working directory: {}'.format(os.getcwd()))
print('INFURA_API_KEY: {}'.format(INFURA_API_KEY))

def getVoteData():
    print('Getting vote data')
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address='0xEa6556E350cD0C61452a26aB34E69EBf6f1808BA', abi=trollboxJson['abi'])
    filter = trollboxContract.events.VoteOccurred.createFilter(fromBlock="0x0")

    events = filter.get_all_entries()
    processedVoteEvents = [{**x, **x['args']} for x in events]

    df = pd.DataFrame(processedVoteEvents)
    df.drop(['args', 'metadata'], axis='columns', inplace=True)

    print('Computing voice credits used per vote')
    df['voiceCreditsUsed'] = df['weights'].apply(lambda x: sum([y ** 2 for y in x]))
    df['choicesMade'] = df['choices'].apply(lambda x: len(x))

    voiceCreditMap = {}
    tournamentId = 1

    def getVoiceCredits(voterId):
        if voterId in voiceCreditMap:
            return voiceCreditMap[voterId]
        else:
            print('Querying chain for voice credits for voter id {}'.format(voterId))
            vc = trollboxContract.functions.getVoiceCredits(tournamentId, voterId).call()
            voiceCreditMap[voterId] = vc
            return vc

    df['currentVoiceCredits'] = df['voterId'].apply(getVoiceCredits)
    df2 = pd.DataFrame([getTransactionGas(row) for _, row in df.iterrows()])

    return pd.concat([df, df2], axis='columns')

def getReferralData():
    print('Getting referral data')
    referralJson = json.load(open('./src/truffle/build/contracts/ReferralProgram.json', 'r'))
    referralContract = w3.eth.contract(address='0x798A709E12CcA28bFa7Ff4a6dAa044b5e0B5FA00', abi=referralJson['abi'])
    filter = referralContract.events.ReferralMade.createFilter(fromBlock="0x0")
    events = filter.get_all_entries()
    processedReferralEvents = [{**x, **x['args']} for x in events]

    df = pd.DataFrame(processedReferralEvents)
    map = {x['referee']: x['referer'] for _, x in df.iterrows()}
    return map


def getTransactionGas(row):
    print('getting transaction for {}'.format(row))
    rcpt = w3.eth.getTransactionReceipt(row['transactionHash'])
    tx = w3.eth.getTransaction(row['transactionHash'])
    weiSpent = rcpt['gasUsed'] * tx['gasPrice']
    ethSpent = w3.fromWei(weiSpent, 'ether')
    return {'gasUsed': rcpt['gasUsed'], 'gasPrice': tx['gasPrice'], 'ethSpent': ethSpent}


def getVotingPower():
    print('getting voting power')
    identityJson = json.load(open('./src/truffle/build/contracts/Identity.json', 'r'))
    identityContract = w3.eth.contract(address='0xf779cae120093807985d5F2e7DBB21d69be6b963', abi=identityJson['abi'])
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address='0xEa6556E350cD0C61452a26aB34E69EBf6f1808BA', abi=trollboxJson['abi'])

    tournamentId = 1
    voiceCredits = []
    numIdentities = identityContract.functions.numIdentities().call()
    for voterId in range(1, numIdentities + 1):
        vc = trollboxContract.functions.getVoiceCredits(tournamentId, voterId).call()
        owner = identityContract.functions.ownerOf(voterId).call()
        voiceCredits.append({
            'voiceCredits': vc,
            'voterId': voterId,
            'owner': owner
        })

    return pd.DataFrame(voiceCredits)

voiceCredits = getVotingPower()
voiceCredits.to_csv('./voice-data.csv', index=False)

# voteDf = getVoteData()
# referralMap = getReferralData()
#
# print('Linking referrals to votes')
# voteDf['referer'] = voteDf.apply(lambda x: referralMap.get(x['voterId'], 0), axis='columns')
#
# print('Writing to disk')
# voteDf.to_csv('./vote-data.csv', index=False)
# print('Done')